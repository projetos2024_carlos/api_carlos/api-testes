module.exports = class alunoController {
   static async postAluno(req, res) {
     const { nome, idade, profissão, cursoMatriculado } = req.body;
 
     console.log(nome+" "+idade+" "+ profissão+" "+ cursoMatriculado+" ");
 
     return res.status(201).json({ message: " Aluno Cadastrado" });
   }
   static async putAluno(req,res) {
    const {nome,idade,profissao,cursoMatriculado} = req.body;
    if (nome != "" && idade > 0 && profissao != "" && cursoMatriculado != "") {
      return res.status(201).json({ message: "Aluno Editado!"})
    }
    else {
      return res.status(400).json({ message: "Aluno com informações faltantes para a edição!"})
    }
   }
   static async deleteAluno(req,res) {
    const alunoId = req.params.id;
    if (alunoId) {
      return res.status(200).json({ message: "Aluno deletado!"})
    }
    else {
      return res.status(400).json({ message: "Não foi fornecido o ID necessário!"})
    }
   }
 };
 
 