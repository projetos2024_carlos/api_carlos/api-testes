const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceHolderController = require('../controller/JSONPlaceHolderController')

router.get('/teacher/', teacherController.getTeachers);
router.post ('/cadastroaluno/', alunoController.postAluno);
router.put('/updatealuno/', alunoController.putAluno);
router.delete('/deletealuno/:id', alunoController.deleteAluno);

router.get('/external/', JSONPlaceHolderController.getUsers);
router.get('/external.io/', JSONPlaceHolderController.getUsersWebsiteIO);
router.get('/external.com/', JSONPlaceHolderController.getUsersWebsiteCOM);
router.get('/external.net/', JSONPlaceHolderController.getUsersWebsiteNET);
router.get('/external/filter', JSONPlaceHolderController.getCountDomain);

module.exports = router